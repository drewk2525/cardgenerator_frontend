# hello-world

## Hit card generator endpoint
### Get Component by ID
`https://yccwbgksyg.execute-api.us-east-1.amazonaws.com/dev/getComponentById`
### Body
{
	"componentId": "1"
}

### Save Component
`https://yccwbgksyg.execute-api.us-east-1.amazonaws.com/dev/saveComponent`
### Body
See component.json in testData directory

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
