import Vue from 'vue';
import VueRouter from 'vue-router';
import GetComponent from '../components/GetComponent.vue';
import CreateComponent from '../components/CreateComponent.vue';
import CreateComponentSet from '../components/CreateComponentSet.vue';
import SaveAsset from '../components/SaveAsset.vue';
import Home from '../components/Home.vue';

Vue.use(VueRouter);

const routes = [
    { component: GetComponent, path: '/getComponent', name: 'Get Component' },
    { component: CreateComponent, path: '/createComponent', name: 'Create Component' },
    { component: CreateComponentSet, path: '/createComponentSet', name: 'Create Component Set' },
    { component: SaveAsset, path: '/saveAsset', name: 'Create Component Set' },
    { component: Home, path: '/', name: 'Home' }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
